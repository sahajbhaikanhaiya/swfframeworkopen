
Pod::Spec.new do |s|

  s.name         = "SWFFrameworkOpen"
  s.version      = "0.0.1"
  s.summary      = "A short description of SWFFrameworkOpen."

  s.description  = "A large description of SWFFrameworkOpen. Used For Amazon Swf"

  s.homepage     = "https://sahajbhaikanhaiya@bitbucket.org/sahajbhaikanhaiya/swfframeworkopen"

  s.license      = "MIT"

  s.author        = { "SWFFrameworkOpen" => "sahaj.bhaikanhaiya@gmail.com" }
 

  s.source       = { :git => "https://sahajbhaikanhaiya@bitbucket.org/sahajbhaikanhaiya/swfframeworkopen.git", :tag => s.version }

  s.ios.deployment_target = '9.0'

  # s.frameworks = 'SWFFrameworkOpen'

  s.source_files  = "SWFFrameworkOpen", "SWFFrameworkOpen/*.{h,m,swift}"
  s.dependency 'SWFFramework'
  # s.dependency 'Alamofire', '~> 4.5'

  # s.dependency 'CryptoSwift'

end
