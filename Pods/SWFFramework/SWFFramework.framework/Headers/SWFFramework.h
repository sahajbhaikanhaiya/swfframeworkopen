//
//  SWFFramework.h
//  SWFFramework
//
//  Created by Singh on 2018-01-02.
//  Copyright © 2018 Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for SWFFramework.
FOUNDATION_EXPORT double SWFFrameworkVersionNumber;

//! Project version string for SWFFramework.
FOUNDATION_EXPORT const unsigned char SWFFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SWFFramework/PublicHeader.h>

