Pod::Spec.new do |s|

    s.name                  = 'SWFFramework'

    s.version               = '0.1.0'

    s.summary               = 'SWFFramework. Pod to add framework to any project.'

    s.description           = "This is a private cocoapod framework for SWF Api Gateway calls."

    s.homepage              = 'https://github.com'

    s.license               = { :type => 'MIT', :file => 'LICENSE'}

    s.author                = { 'sahajbhaikanhaiya' => 'sahaj.bhaikanhaiya@gmail.com' }

    s.source                = { :http => 'https://bitbucket.org/sahajbhaikanhaiya/swfframeworkzip/raw/e9c99b0b1840c2a0200a91a0ce4d5223adaa66f8/SWFFramework.framework.zip', :flatten => true }

    s.ios.deployment_target = '9.0'

    s.frameworks = 'SWFFramework'

    s.vendored_frameworks = 'SWFFramework.framework'

end
